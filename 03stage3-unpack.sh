#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

source config
source $GI_CHROOT/config.tmp

cd $GI_CHROOT || exit 1
echo -e "\e[1;34mUNPACK STAGE3\e[00m"
tar --checkpoint=10000 -xjpf $STAGE3_ARCHIVE --xattrs || exit 1
