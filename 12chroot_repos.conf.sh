#!/bin/bash
source config
trap 'echo "ERROR! Exiting..."; exit 1' ERR
mkdir $GI_CHROOT/etc/portage/repos.conf
cp $GI_CHROOT/usr/share/portage/config/repos.conf $GI_CHROOT/etc/portage/repos.conf/gentoo.conf
