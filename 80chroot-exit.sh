#!/bin/bash
source config

set -x

umount -f $GI_CHROOT/proc
umount -f $GI_CHROOT/dev/pts
umount -f $GI_CHROOT/dev/shm
umount -f $GI_CHROOT/dev
umount -f $GI_CHROOT/sys

set +x
