#!/bin/bash
source config

mount -t proc proc $GI_CHROOT/proc || exit 1
mount --rbind /sys $GI_CHROOT/sys || exit 1
mount --make-rslave $GI_CHROOT/sys || exit 1            # for systemd
mount --rbind /dev $GI_CHROOT/dev || exit 1
mount --make-rslave $GI_CHROOT/dev || exit 1            # for systemd
