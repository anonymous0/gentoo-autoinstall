#!/bin/bash
echo "Europe/Kiev" > /etc/timezone
emerge --config sys-libs/timezone-data

echo "uk_UA.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

echo "LANG=\"uk_UA.UTF-8\"" > /etc/env.d/02locale
echo "LC_COLLATE=\"C\"" >> /etc/env.d/02locale

env-update && source /etc/profile
