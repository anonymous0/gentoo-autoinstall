#!/bin/bash
euse -D bindist
euse -E gnutls
euse -E symlink
euse -p sys-apps/man-db -D gdbm
euse -p dev-libs/gmp -E pgo
euse -p dev-libs/openssl -E gmp
euse -p dev-vcs/git -D perl
