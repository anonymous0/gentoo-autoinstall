#!/bin/bash
trap 'echo "ERROR! Exiting..."; exit 1' ERR
source /etc/profile
env-update
export PS1="(chroot) $PS1"
