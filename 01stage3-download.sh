#!/bin/bash
source config

cd $GI_CHROOT || exit 1

case $GI_ARCH in
	'x86')
		GI_PLATFORM='i686' ;;
	* )
		GI_PLATFORM=$GI_ARCH ;;
esac

STAGE3=$(wget -qO - http://distfiles.gentoo.org/releases/$GI_ARCH/autobuilds/latest-stage3-$GI_PLATFORM-hardened.txt | grep -v '#' | cut -d' ' -f1)
echo -e "\e[1;34mLatest stage3: \e[00;31m$STAGE3\e[00m"

STAGE3_ARCHIVE=`echo $STAGE3 | grep -oP "stage3.*"`
echo "STAGE3_ARCHIVE=\"$STAGE3_ARCHIVE\"" > $GI_CHROOT/config.tmp

wget -vc "http://distfiles.gentoo.org/releases/$GI_ARCH/autobuilds/$STAGE3" -O $GI_CHROOT/$STAGE3_ARCHIVE
wget -vc "http://distfiles.gentoo.org/releases/$GI_ARCH/autobuilds/$STAGE3.DIGESTS" -O $GI_CHROOT/$STAGE3_ARCHIVE.DIGESTS
wget -vc "http://distfiles.gentoo.org/releases/$GI_ARCH/autobuilds/$STAGE3.DIGESTS.asc" -O $GI_CHROOT/$STAGE3_ARCHIVE.DIGESTS.asc
wget -vc "http://distfiles.gentoo.org/releases/$GI_ARCH/autobuilds/$STAGE3.CONTENTS" -O $GI_CHROOT/$STAGE3_ARCHIVE.CONTENTS
