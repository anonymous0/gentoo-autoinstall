#!/bin/bash
source config
source $GI_CHROOT/config.tmp

cd $GI_CHROOT || exit 1
echo -e "\e[1;34mCHECK FILES SHA512\e[00m"
grep -A 1 SHA512 $GI_CHROOT/$STAGE3_ARCHIVE.DIGESTS | grep "stage3" | sha512sum -c - || exit 1

#TODO openssl dgst -r -whirlpool stage3-amd64-<release>.tar.bz2
